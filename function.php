<?php

session_start();

function getconn(){

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "abhidb";

// Create connection
$conn =  mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
  die("Connection failed: " . mysqli_connect_error());
}
return  $conn;
}

function authorization() {

    if (!isset($_SESSION["username"]))  {
      header("location:login_form.php");
    }

}